# Oersetter Webservice

## Installation & Usage

1. Make a Python virtual environment: ``$ python3 -m venv env``
2. Activate the environment: ``$ source env/bin/activate``
3. Clone this repository, make sure to clone with ``--recursive`` so all submodules are cloned! Otherwise, things will
   fail later.
4. The models are not included and need to be downloaded separately by invoking the following script:
   ``cd pipeline && ./download-models.sh && cd -``
5. Run ``./startserver_development.sh``
6. Navigate your webbrowser to the specified host and port ( usually http://127.0.0.1:8080 )

Or, with docker, step 3 and 4 and then:

For development:

```
docker build . -t oersetter-test
# REVIEW compose file!!
docker swarm init
docker stack deploy -c docker-compose.yml oersetter-test
```

For production:

```
export APPNAME=oersetter3
export VOLUMEROOT=${HOME}/volumes
docker build -f Dockerfile.ubu2024 -t ${APPNAME} .
mkdir -p ${VOLUMEROOT}/${APPNAME}/config
cp oersetter.ini ${VOLUMEROOT}/${APPNAME}/config
cp oersetter/functions.py ${VOLUMEROOT}/${APPNAME}
cp startserver_production.sh ${VOLUMEROOT}/${APPNAME}
# REVIEW compose file, oersetter.ini and startserver_production.sh must be volumes!!
docker swarm init
docker stack deploy -c docker-compose.yml oersetter2
```

The development container exposes a webserver on port 8080, the production container exposes a uwsgi interface (*not an
HTTP server!*) on 8080.
To connect this, configure your reverse proxy as follows (example using Apache, make sure you have ``mod_proxy_uwsgi``
installed and enabled on your reverse proxy for this to work):

```
ProxyPass / uwsgi://1.2.3.4:8080/
```

(change the IP to the actual IP of the container).

Note the forceurl in oersetter.config.yml / docker-compose.yml, although formally not required, this offers a solution in case the autodetection mechanism doesn't work as expected.


