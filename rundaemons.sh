#/usr/bin/env bash

# This script starts the necessary background daemons
# and remains running until either background daemon
# stops. A proper exit code will be returned.

ROOT=$(dirname $(realpath $0))/pipeline

if [ ! -e "$ROOT/data/models" ]; then
    echo "Models not downloaded yet! doing so now..."
    $ROOT/pipeline/download-models.sh || exit 1
fi


startdaemon() {
    echo "Starting marian-server $SRC-$TGT on port $PORT" >&2
    marian-server --port $PORT -c $ROOT/data/models/$MODELNAME/$SRC-$TGT.model.npz.decoder.yml -m $ROOT/data/models/$MODELNAME/$SRC-$TGT.model.npz -d $GPUS -b $BEAMSIZE -w 6000 --normalize 0.6 --mini-batch 64 --maxi-batch-sort src --maxi-batch 100 --allow-unk 2> $ROOT/data/log/server-$SRC-$TGT.log &
}

MODELNAME=exp6 #also adapt in functions.py!

PORT=8081 #must correspond to the same numbers in functions.py
SRC="fy"
TGT="nl"
GPUS=0 #we do cpu only
BEAMSIZE=12
startdaemon
PID1=$!


PORT=8082 #must correspond to the same numbers in functions.py
SRC="nl"
TGT="fy"
GPUS=0 #we do cpu only
BEAMSIZE=12
startdaemon
PID2=$!

echo "Background daemons: $PID1 $PID2">&2

#wait for any subprocesses to finish and return (-n this is a bashism, not POSIX! requires bash>=4.3)
wait -n $PID1 $PID2
EXIT=$?
#kill remaining processes (one will already be killed so silence stderr)
kill $PID1 $PID2 2> /dev/null
exit $EXIT
