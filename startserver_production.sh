#!/bin/bash
cd /usr/src/oersetter2-webservice
./rundaemons.sh &
PID1=$!
# make sure oersetter.ini is present (via docker volume)
uwsgi oersetter.ini &
PID2=$!

#wait for any subprocesses to finish and return (-n this is a bashism, not POSIX! requires bash>=4.3)
wait -n $PID1 $PID2
EXIT=$?
if [ $EXIT -ne 0 ]; then
    echo "== Exited with failure ($EXIT), last log excerpts ==" >&2
    tail /var/log/uwsgi/oersetter.uwsgi.log pipeline/data/log/server-fy-nl.log pipeline/data/log/server-nl-fy.log >&2
fi
#kill remaining processes (one will already be killed so silence stderr)
kill $PID1 $PID2 2> /dev/null
exit $EXIT

