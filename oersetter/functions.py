import re
from os import replace

import ucto
import subprocess
import os.path

from websocket import create_connection
from werkzeug.exceptions import ServiceUnavailable

BATCH_SIZE=1
MODELNAME="exp6" #also adapt in rundaemons.sh!
DATADIR="." #will be injected by caller

PORT_FY2NL = 8081
PORT_NL2FY = 8082

def translate(port: int, text: str):
    try:
        ws = create_connection("ws://localhost:{}/translate".format(port))
    except Exception as e:
        raise ServiceUnavailable("Unable to connect to the translation server") from e

    count = 0
    batch = ""
    for line in text.split("\n"):
        count += 1
        batch += line + "\n"
        if count == BATCH_SIZE:
            ws.send(batch)
            result = ws.recv()
            yield detokenize(resolve_subtokens(result.rstrip()))
            count = 0
            batch = ""

    if count:
        ws.send(batch)
        result = ws.recv()
        yield detokenize(resolve_subtokens(result.rstrip()))

    ws.close()

def tokenize(text: str, configuration) -> str:
    tokenizer = ucto.Tokenizer(configuration)
    tokenizer.process(text)
    toktext=""
    for token in tokenizer:
        if token.isnewparagraph():
            toktext += "\n\n" + str(token)
        elif token.isendofsentence():
            toktext += str(token) + "\n"
        else:
            toktext += str(token)
        toktext += " "
    return toktext

def subtokenize(text: str, modelname: str) -> str:
    result = subprocess.run(["subword-nmt", "apply-bpe","-c",os.path.join(DATADIR, f"models/{modelname}/nl-fy.bpe")], input=text + "\n", capture_output=True, encoding='utf-8', check=False) #note: the BPE-encoded vocabulary is language independent (contains both dutch and frisian)
    if result.returncode != 0: #manual check
        raise ServiceUnavailable("Subword tokenisation failed: " + str(result.stderr))
    return result.stdout.strip()


def detokenize(text: str) -> str:
    text = re.sub(r' ([.,:;?!%]+)', r"\1", text)
    text = text.replace(". . .","...")
    text = text.replace("( ", "(").replace(" )", ")")
    text = text.replace("{ ", "{").replace(" }", "}")
    text = text.replace("‘ ","‘").replace(" ’","’")
    text = re.sub(r" ' ?([tsnke])",r"č\1",text)
    return text

def resolve_subtokens(text: str) -> str:
    return text.replace("@@ ","")

def stripNewLines(text: str) -> str:
    return re.sub(r"' ([^']+) '",r"'\1'",re.sub(r'" ([^"]+) "',r'"\1"',re.sub(r"([^×])×",r"\1 ",text).replace("×","\n"))).replace("č","'")

def fy2nl(text: str) -> str:
    return stripNewLines("×".join(translate(PORT_FY2NL, subtokenize(tokenize(text, "/usr/src/uctodata/config/tokconfig-fry"), MODELNAME))))

def nl2fy(text: str) -> str:
    return stripNewLines("×".join(translate(PORT_NL2FY, subtokenize(tokenize(text, "/usr/src/uctodata/config/tokconfig-nld"), MODELNAME))))
