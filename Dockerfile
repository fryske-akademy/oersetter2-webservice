FROM ubuntu:24.04
MAINTAINER Maarten van Gompel <proycon@anaproy.nl>
LABEL description="Oersetter"
ARG forceurl=""
ENV TZ=Europe/Amsterdam
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get -y update && \
apt-get -y install git sudo gcc g++ autoconf-archive autotools-dev make libtool build-essential libboost-all-dev protobuf-compiler libprotobuf-dev openssl libssl-dev libxml2-dev libicu-dev libbz2-dev zlib1g-dev libtar-dev libexttextcat-dev libgoogle-perftools-dev gawk sed grep gnupg2 curl ca-certificates perl python3 cython3 python3-pip python3-lxml python3-requests python3-websocket cmake intel-mkl uwsgi uwsgi-plugin-python3 dumb-init

RUN mkdir -p /usr/src/

#Marian-NMT
RUN cd /usr/src && git clone https://github.com/marian-nmt/marian-dev.git marian && \
cd /usr/src/marian && git checkout 1.12.0 && mkdir -p build && cd build && cmake .. -DUSE_SENTENCEPIECE=on -DCOMPILE_SERVER=on -DCOMPILE_CUDA=off -DUSE_DOXYGEN=off -DCOMPILE_CPU=on && make -j 4 && \
cp /usr/src/marian/build/marian* /usr/local/bin/ && \
cp /usr/src/marian/build/spm_* /usr/local/bin/

#Moses SMT (we don't need the decoder technically but we do need some of its scripts)
RUN cd /usr/src && git clone https://github.com/moses-smt/mosesdecoder

#Subword Neural Machine Translation (preprocessing to segment text into subword units)
RUN sudo pip3 install --break-system-packages subword-nmt python-ucto==0.6.8

# uctodata
RUN sudo apt install wget
RUN sudo python3 -c "import ucto; ucto.installdata()"
# TODO this we want to get rid of
RUN sudo mkdir /usr/src/uctodata && mv ~/.config/ucto /usr/src/uctodata/config

#CLAM
RUN sudo pip3 install --break-system-packages clam

#Set locales
RUN apt-get -y install language-pack-en language-pack-nl
ENV LC_ALL=en_US.UTF-8

RUN chown 33:33 /var/log/uwsgi

RUN mkdir -p /usr/src/oersetter2-webservice
WORKDIR /usr/src/oersetter2-webservice

COPY . /usr/src/oersetter2-webservice/

#Set forceurl in configuration, if passed at build time:
RUN if [ -n "$forceurl" ]; then echo "forceurl: \"$forceurl\"" >> /usr/src/oersetter2-webservice/oersetter.config.yml; fi

RUN cd /usr/src/oersetter2-webservice && python3 setup.py install

CMD cd /usr/src/oersetter2-webservice && ./startserver_development.sh
